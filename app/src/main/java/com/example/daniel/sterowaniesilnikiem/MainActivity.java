package com.example.daniel.sterowaniesilnikiem;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private static final int ENABLE_BT_REQUEST_CODE = 1;

    int sequence_data[][] = {{0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}};
    RotateAnimation rotate_left_animation;
    RotateAnimation rotate_right_animation;
    byte rotate_direction = 0;
    byte prev_roatete_direction = 0;
    byte sequence_status = 0;
    ImageButton btn_left = null;
    ImageButton btn_right = null;
    ImageButton btn_cycle = null;
    ImageButton btn_auto = null;
    String address = null;
    TextView rotate_text;
    private ProgressDialog progress;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private Thread t1 = null;
    private Thread t2 = null;
    private boolean stop_sendig_thread = false;
    int data_sequence[][] = {{0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}};
    ProgressDialog wait_dialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent newint = getIntent();
        address = newint.getStringExtra(SelectActivity.EXTRA_ADDRESS);
        new ConnectBT().execute();

        btn_left = (ImageButton) findViewById(R.id.imageButton);
        btn_right = (ImageButton) findViewById(R.id.imageButton2);
        btn_cycle = (ImageButton) findViewById(R.id.imageButton3);
        btn_auto = (ImageButton) findViewById(R.id.imageButton4);
        rotate_text = (TextView) findViewById(R.id.txt1);

        rotate_right_animation = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        rotate_right_animation.setDuration(1000);
        rotate_right_animation.setInterpolator(new LinearInterpolator());
        rotate_right_animation.setRepeatCount(-1);
        rotate_left_animation = new RotateAnimation(360.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        rotate_left_animation.setDuration(1000);
        rotate_left_animation.setInterpolator(new LinearInterpolator());
        rotate_left_animation.setRepeatCount(-1);


        btn_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (rotate_direction == 1) {
                    sendSignal(0x01, 0, 1);
                }
                else {
                    sendSignal(0x02, 0, 1);
                }
            }
        });

        btn_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (rotate_direction == 2) {
                    sendSignal(0x01, 0, 1);
                }
                else {
                    sendSignal(0x03, 0, 1);
                }
            }
        });

        btn_cycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {

                wait_dialog = ProgressDialog.show(MainActivity.this , "Pobieranie danych", "Proszę czekać");
                Thread t = new Thread(new Runnable() {

                    @Override
                    public void run() {
                    stop_sendig_thread = true;
                    for (int i = 0; i < 10; i++){
                        try {
                            int function = 0x1e + i;
                            sendSignal(function, 0x00, 0x00);
                            Thread.sleep(50);
                            function = 0x28 + i;
                            sendSignal(function, 0x00, 0x00);
                            Thread.sleep(50);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    stop_sendig_thread = false;
                    }
                });
                t.start();


            }
        });


//
//        btnDis.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick (View v) {
////                Calendar rightNow = Calendar.getInstance();
////                sendSignal(0x07, rightNow.get(Calendar.HOUR_OF_DAY) , rightNow.get(Calendar.MINUTE));
////                sendSignal(0x08, rightNow.get(Calendar.SECOND),rightNow.get(Calendar.DAY_OF_MONTH));
////                sendSignal(0x09, rightNow.get(Calendar.MONTH) + 1, 19);
//                //Disconnect();
//                t1.stop();
//                t2.stop();
//            }start a
//        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == RESULT_OK){
            String data_read = data.getStringExtra("result");
            String part1[] = data_read.split(":");
            int int_data_read[][] = {{0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}};

            for (int i = 0; i < 10; i++){
                String part2[] = part1[i].split("a");
                int_data_read[i][0] = Integer.parseInt(part2[0]);
                int_data_read[i][1] = Integer.parseInt(part2[1]);
            }
            sequence_data = int_data_read;
            sendSequence();
        }

    }

    private void changeAnimation(){
        if (rotate_direction != prev_roatete_direction){
            btn_left.clearAnimation();
            btn_right.clearAnimation();
            if (rotate_direction == 1){
                this.btn_left.startAnimation(this.rotate_left_animation);
            }
            else if (rotate_direction == 2){
                this.btn_right.startAnimation(this.rotate_right_animation);
            }
            prev_roatete_direction = rotate_direction;
        }
    }

    public void button_auto_click (View view){
        if (sequence_status == 0){
            sendSignal(0x19, 0x00, 0x01);
            btn_auto.setImageResource(R.drawable.auto_run);
        }
        else {
            sendSignal(0x1a , 0x00, 0x01);
            btn_auto.setImageResource(R.drawable.auto_stop);
        }

    }

    private void sendSignal(int function, int data1, int data2) {
        if ( btSocket != null ) {
            try {
                btSocket.getOutputStream().write(0x01);
                btSocket.getOutputStream().write((byte)function);
                btSocket.getOutputStream().write((byte)data1);
                btSocket.getOutputStream().write((byte)data2);
                btSocket.getOutputStream().write(0x04);
            } catch (IOException e) {
                //msg("Error");
            }
        }
    }

    private void sendSequence() {
        Thread t3 = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    stop_sendig_thread = true;
                    for (int i = 0; i < 10; i++){
                        sendSignal(0x05 + i, sequence_data[i][1] / 255, sequence_data[i][1] % 255);
                        Thread.sleep(50);
                        sendSignal(0x0f + i, 0x00, sequence_data[i][0]);
                        Thread.sleep(50);
                    }
                    stop_sendig_thread = false;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t3.start();
    }


    private void bluetooth_response(byte function, byte data1_byte, byte data2_byte)
    {
        int data1 = 0;
        int data2 = 0;
        if (data1_byte < 0) data1 = (data1_byte + 256); else data1 = (int) data1_byte;
        if (data2_byte < 0) data2 = (data2_byte + 256); else data2 = (int) data2_byte;

        int one_data = (data1*256) + data2;

        if (function == 0x1b){
            rotate_text.setText(String.valueOf(one_data)+ " RPM");
        }
        if (function == 0x1c){
            rotate_direction = (byte) one_data;
            changeAnimation();
        }
        if (function == 0x1d) {
            sequence_status = (byte) one_data;
            if (one_data > 0){
                btn_auto.setImageResource(R.drawable.auto_run);
            }
            else{
                btn_auto.setImageResource(R.drawable.auto_stop);
            }
        }

        if (function >= 0x1e && function <= 0x27){
            int index = function - 0x1e;
            data_sequence[index][1] = one_data;

        }

        if (function >= 0x28 && function <= 0x31){
            int index = function - 0x28;
            data_sequence[index][0] = one_data;
            if (function == 0x31){
                String to_return = "";
                for (int i = 0; i < 10; i++){
                    to_return = to_return + String.valueOf(data_sequence[i][0]) + "a" + String.valueOf(data_sequence[i][1]) + ":";
                }
                Log.d("abecede", to_return);
                Intent i = new Intent(MainActivity.this, SequenceActivity.class);
                i.putExtra("data", to_return);
                startActivityForResult(i, 1);
                wait_dialog.dismiss();
            }
        }


    }

//    void sendSignal(byte function, byte data1, byte data2) {
//
//    }

    private void Disconnect () {
        if ( btSocket!=null ) {
            try {
                btSocket.close();
            } catch(IOException e) {
                msg("Error");
            }
        }

        finish();
    }

    private void msg (String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }


    private class ConnectBT extends AsyncTask<Void, Void, Void> {
        private boolean ConnectSuccess = true;

        @Override
        protected  void onPreExecute () {
            progress = ProgressDialog.show(MainActivity.this, "Connecting...", "Please Wait!!!");
        }

        @Override
        protected Void doInBackground (Void... devices) {
            try {
                if ( btSocket==null || !isBtConnected ) {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();

                    t1 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            byte[] buffer = new byte[256];
                            int bytes;
                            byte raw_data[] = new byte[5];
                            byte data_read[] = new byte[256];
                            while(true){
                                try {
                                    bytes = btSocket.getInputStream().read(raw_data, 0, 5);
                                    String readMessage = new String(buffer, 0, bytes);
                                    //raw_data = buffer;

//                                    for (int i = 0; i < readMessage.length(); i++) {
//                                        char c = readMessage.charAt(i);
//                                        byte x = (byte) c;
//                                        raw_data[i] = x;
//                                    }

                                    for (int x = 0; x < readMessage.length(); x++)
                                    {
                                        byte copy_data_read[] = new byte[5];
                                        for (int i = 0; i < 4; i++) {
                                            copy_data_read[i] = data_read[i + 1];
                                        }
                                        copy_data_read[4] = raw_data[x];
                                        data_read=copy_data_read;
                                    }

                                    if (data_read[0] == 1 && data_read[4] == 4)
                                    {
                                        bluetooth_response(data_read[1], data_read[2], data_read[3]);
                                    }

                                } catch (IOException e) {
                                    //break;
                                }
                            }
                        }
                    });

                    t2 = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            while (true){
                                try {
                                    Calendar rightNow = Calendar.getInstance();
                                    Thread.sleep(100);
                                    if (!stop_sendig_thread) sendSignal(0x1b, 0x00, 0x00);
                                    Thread.sleep(100);
                                    if (!stop_sendig_thread) sendSignal(0x1c, 0x00, 0x00);
                                    Thread.sleep(100);
                                    if (!stop_sendig_thread) sendSignal(0x1d, 0x00, 0x00);
                                    Thread.sleep(100);
                                    if (!stop_sendig_thread) sendSignal(0x32, rightNow.get(Calendar.HOUR_OF_DAY), rightNow.get(Calendar.MINUTE));
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });

                    t1.start();
                    t2.start();
                }
            } catch (IOException e) {
                ConnectSuccess = false;
            }

            return null;
        }


        @Override
        protected void onPostExecute (Void result) {
            super.onPostExecute(result);

            if (!ConnectSuccess) {
                msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
                finish();
            } else {
                msg("Connected");
                isBtConnected = true;
            }

            progress.dismiss();
        }
    }


}
