package com.example.daniel.sterowaniesilnikiem;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class SequenceActivity extends AppCompatActivity {

    Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;
    RadioGroup grp0, grp1, grp2, grp3, grp4, grp5, grp6, grp7, grp8, grp9;
    EditText txt0, txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9;
    int data[][] = {{0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sequence);

        grp0 = (RadioGroup) findViewById(R.id.grp0);
        grp1 = (RadioGroup) findViewById(R.id.grp1);
        grp2 = (RadioGroup) findViewById(R.id.grp2);
        grp3 = (RadioGroup) findViewById(R.id.grp3);
        grp4 = (RadioGroup) findViewById(R.id.grp4);
        grp5 = (RadioGroup) findViewById(R.id.grp5);
        grp6 = (RadioGroup) findViewById(R.id.grp6);
        grp7 = (RadioGroup) findViewById(R.id.grp7);
        grp8 = (RadioGroup) findViewById(R.id.grp8);
        grp9 = (RadioGroup) findViewById(R.id.grp9);

        txt0 = (EditText) findViewById(R.id.txt0);
        txt1 = (EditText) findViewById(R.id.txt1);
        txt2 = (EditText) findViewById(R.id.txt2);
        txt3 = (EditText) findViewById(R.id.txt3);
        txt4 = (EditText) findViewById(R.id.txt4);
        txt5 = (EditText) findViewById(R.id.txt5);
        txt6 = (EditText) findViewById(R.id.txt6);
        txt7 = (EditText) findViewById(R.id.txt7);
        txt8 = (EditText) findViewById(R.id.txt8);
        txt9 = (EditText) findViewById(R.id.txt9);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            String data_read = bundle.getString("data");
            Log.d("abecede", data_read);
            String part1[] = data_read.split(":");
            int int_data_read[][] = {{0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}};

            for (int i = 0; i < 10; i++) {
                String part2[] = part1[i].split("a");
                if (Integer.parseInt(part2[0]) == 0){
                    int_data_read[i][0] = 3;
                }
                else {
                    int_data_read[i][0] = Integer.parseInt(part2[0]);
                }
                int_data_read[i][1] = Integer.parseInt(part2[1]);
            }
            data = int_data_read;
        }
        txt0.setText(String.valueOf(data[0][1]));
        txt1.setText(String.valueOf(data[1][1]));
        txt2.setText(String.valueOf(data[2][1]));
        txt3.setText(String.valueOf(data[3][1]));
        txt4.setText(String.valueOf(data[4][1]));
        txt5.setText(String.valueOf(data[5][1]));
        txt6.setText(String.valueOf(data[6][1]));
        txt7.setText(String.valueOf(data[7][1]));
        txt8.setText(String.valueOf(data[8][1]));
        txt9.setText(String.valueOf(data[9][1]));

        for (int i = 0; i < 10; i++)
        {
            int id = getResources().getIdentifier("g" + String.valueOf(i) + String.valueOf(data[i][0]), "id", this.getPackageName());
            int id2 = getResources().getIdentifier("grp" + String.valueOf(i), "id", this.getPackageName());
            RadioGroup radiogrp = (RadioGroup) findViewById(id2);
            radiogrp.check(id);
        }

    }
    public void onAccept(View view){






                int selectedId = grp0.getCheckedRadioButtonId();
                RadioButton radiobtn = (RadioButton) findViewById(selectedId);
                int direction = 0;
                if (radiobtn.getText().equals("Lewo")) direction = 1;
                if (radiobtn.getText().equals("Prawo")) direction = 2;
                int value = 0;
                try {
                    value = Integer.parseInt(String.valueOf(txt0.getText()));
                }
                catch (NumberFormatException ex){
                }
                data[0][0] = direction;
                data[0][1] = value;

                selectedId = grp1.getCheckedRadioButtonId();
                radiobtn = (RadioButton) findViewById(selectedId);
                direction = 0;
                if (radiobtn.getText().equals("Lewo")) direction = 1;
                if (radiobtn.getText().equals("Prawo")) direction = 2;
                value = 0;
                try {
                    value = Integer.parseInt(String.valueOf(txt1.getText()));
                }
                catch (NumberFormatException ex){
                }
                data[1][0] = direction;
                data[1][1] = value;

                selectedId = grp2.getCheckedRadioButtonId();
                radiobtn = (RadioButton) findViewById(selectedId);
                direction = 0;
                if (radiobtn.getText().equals("Lewo")) direction = 1;
                if (radiobtn.getText().equals("Prawo")) direction = 2;
                value = 0;
                try {
                    value = Integer.parseInt(String.valueOf(txt2.getText()));
                }
                catch (NumberFormatException ex){
                }
                data[2][0] = direction;
                data[2][1] = value;

                selectedId = grp3.getCheckedRadioButtonId();
                radiobtn = (RadioButton) findViewById(selectedId);
                direction = 0;
                if (radiobtn.getText().equals("Lewo")) direction = 1;
                if (radiobtn.getText().equals("Prawo")) direction = 2;
                value = 0;
                try {
                    value = Integer.parseInt(String.valueOf(txt3.getText()));
                }
                catch (NumberFormatException ex){
                }
                data[3][0] = direction;
                data[3][1] = value;


                selectedId = grp4.getCheckedRadioButtonId();
                radiobtn = (RadioButton) findViewById(selectedId);
                direction = 0;
                if (radiobtn.getText().equals("Lewo")) direction = 1;
                if (radiobtn.getText().equals("Prawo")) direction = 2;
                value = 0;
                try {
                    value = Integer.parseInt(String.valueOf(txt4.getText()));
                }
                catch (NumberFormatException ex){
                }
                data[4][0] = direction;
                data[4][1] = value;

                selectedId = grp5.getCheckedRadioButtonId();
                radiobtn = (RadioButton) findViewById(selectedId);
                direction = 0;
                if (radiobtn.getText().equals("Lewo")) direction = 1;
                if (radiobtn.getText().equals("Prawo")) direction = 2;
                value = 0;
                try {
                    value = Integer.parseInt(String.valueOf(txt5.getText()));
                }
                catch (NumberFormatException ex){
                }
                data[5][0] = direction;
                data[5][1] = value;

                selectedId = grp6.getCheckedRadioButtonId();
                radiobtn = (RadioButton) findViewById(selectedId);
                direction = 0;
                if (radiobtn.getText().equals("Lewo")) direction = 1;
                if (radiobtn.getText().equals("Prawo")) direction = 2;
                value = 0;
                try {
                    value = Integer.parseInt(String.valueOf(txt6.getText()));
                }
                catch (NumberFormatException ex){
                }
                data[6][0] = direction;
                data[6][1] = value;

                selectedId = grp7.getCheckedRadioButtonId();
                radiobtn = (RadioButton) findViewById(selectedId);
                direction = 0;
                if (radiobtn.getText().equals("Lewo")) direction = 1;
                if (radiobtn.getText().equals("Prawo")) direction = 2;
                value = 0;
                try {
                    value = Integer.parseInt(String.valueOf(txt7.getText()));
                }
                catch (NumberFormatException ex){
                }
                data[7][0] = direction;
                data[7][1] = value;

                selectedId = grp8.getCheckedRadioButtonId();
                radiobtn = (RadioButton) findViewById(selectedId);
                direction = 0;
                if (radiobtn.getText().equals("Lewo")) direction = 1;
                if (radiobtn.getText().equals("Prawo")) direction = 2;
                value = 0;
                try {
                    value = Integer.parseInt(String.valueOf(txt8.getText()));
                }
                catch (NumberFormatException ex){
                }
                data[8][0] = direction;
                data[8][1] = value;

                selectedId = grp9.getCheckedRadioButtonId();
                radiobtn = (RadioButton) findViewById(selectedId);
                direction = 0;
                if (radiobtn.getText().equals("Lewo")) direction = 1;
                if (radiobtn.getText().equals("Prawo")) direction = 2;
                value = 0;
                try {
                    value = Integer.parseInt(String.valueOf(txt9.getText()));
                }
                catch (NumberFormatException ex){
                }
                data[9][0] = direction;
                data[9][1] = value;








        String to_return = "";
        for (int i = 0; i < 10; i++){
            to_return = to_return + String.valueOf(data[i][0]) + "a" + String.valueOf(data[i][1]) + ":";
        }
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", to_return);
        setResult(RESULT_OK,returnIntent);
        finish();
    }
}
